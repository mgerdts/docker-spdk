<!-- Copyright 2020 NVIDIA Corporation. All rights reserved. -->

# Docker container for spdk

This repo contains the bits needed to build [spdk](https://spdk.io) with the following enabled:

* Mellanox CX-5 RDMA
* [OCF](https://open-cas.github.io/getting_started_spdk.html)
* [RBD](https://spdk.io/doc/bdev.html#bdev_config_rbd)
* iSCSI initiator

# Build

This is built automatically with gitlab-ci.  It will probably build more quickly locally for test runs.

```
$ cd <base-distro>
$ docker build -t spdk:test
```

# Run the container

This runs the container in a way that will allow it raw access to NVMe and other drives and access to the host (RDMA-capable?) network.

```
docker run --rm --name spdk --hostname spdk --privileged --network=host \
    -v /dev:/dev -v /etc/spdk:/etc/spdk -v /etc/ceph:/etc/ceph \
    gitlab.com/mgerdts/docker-spdk:test
```

The container is configured to run `/opt/spdk/bin/spdk_tgt -m 0xf` by default.  If you would like to get an interactive shell (e.g. to start it under gdb), use `--entrypoint`.  For example:

```
docker run --rm --name spdk --hostname spdk --privileged --network=host \
    -v /dev:/dev -v /etc/spdk:/etc/spdk -v /etc/ceph:/etc/ceph \
    --entrypoint /bin/bash
    gitlab.com/mgerdts/docker-spdk:test
```

If you would like to use different arguments other than `-m 0xf`, you can do so by placing them after the docker image name.

```
docker run --rm --name spdk --hostname spdk --privileged --network=host \
    -v /dev:/dev -v /etc/spdk:/etc/spdk -v /etc/ceph:/etc/ceph \
    gitlab.com/mgerdts/docker-spdk:test -m 0xf00f
```

# Configure spdk

```
$ docker exec spdk /opt/spdk/scripts/rpc.py < /etc/spdk/mumble.config
```

Since `/etc/spdk` is mounted in the container, you could also do that with:

```
[user@host ~]$ docker exec -it spdk bash
[root@spdk /]# /opt/spdk/scripts/rpc.py < /etc/spdk/mumble.config
```

# Extracting bits from the container

This will generate bits in `spdk.tar` that may be extracted from `/` on a target machine.

```
docker run --rm gitlab.com/mgerdts/docker-spdk:$(git rev-parse --short HEAD) \
    bash -c 'cd / && tar cf - opt/fio opt/spdk' > spdk.tar
```

On the target machine:

```
cd /
tar xf ~/spdk.tar
yum install -y $(cat /opt/spdk/spdk.runtime-deps)
```
